<?php
/**
 * Created by PhpStorm.
 * User: Afeez
 * Date: 2/8/2017
 * Time: 12:34 PM
 */
require_once('../include/check.php'); confirmlogin();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0 width=device-width">
    <title>Dashboard</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/animate.min.css">
    <link rel="stylesheet" href="../css/ionicons.min.css">
    <link rel="stylesheet" href="../css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/dashboard.css">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700" rel="stylesheet">
</head>
<body onload="chk('ghostgeneral-hello')">
<nav class="navbar navbar-default navbar-fixed-top " id="dashboard-navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-"
                    aria-expanded="false" id="nav-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar" style="width: 15px"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" id="app-logo-dashboard" href="../index.php">kolowize</a>
        </div>

        <div class="collapse navbar-collapse" id="navbar-collapse-">
            <ul class="nav navbar-nav" id="dashboard-main-nav">
                <li><a class="nav-links" href="#"><i class="pe-7s-graph1"></i> Dashboard</a></li>
                <li role="separator" class="mobile-link-divider"></li>
                <li><a class="nav-links" href="#"><i class="pe-7s-server"></i> My Savings</a></li>
                <li role="separator" class="mobile-link-divider"></li>
                <li><a class="nav-links" href="#"><i class="pe-7s-gift"></i> My Rewards</a></li>
                <li role="separator" class="mobile-link-divider"></li>
                <li><a class="nav-links js-overlay-open" href="#"><i class="pe-7s-wallet"></i> New Savings Plan</a></li>
                <li role="separator" class="mobile-link-divider"></li>
                <li class="hidden-lg hidden-md hidden-sm"><a href="#" class="nav-links"><i class="pe-7s-config"></i>
                        Settings</a></li>
                <li role="separator" class="mobile-link-divider"></li>
                <li class="hidden-lg hidden-md hidden-sm"><a href="index.php" class="nav-links"><i class="pe-7s-power"></i> Sign
                        Out</a></li>

            </ul>
            <ul class="nav navbar-nav navbar-right hidden-xs" id="dashboard-sec-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <i class="pe-7s-user"></i> Hi <?php echo ucwords($_SESSION['namefull']);?>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="dashboard_settings.html" class="sec-nav-links"><i class="pe-7s-config"></i> Settings</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="../logout" class="sec-nav-links"><i class="pe-7s-power"></i> Sign Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<main>
    <div class="activity-summary">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 stats">
                    <div class="text-center">
                        <div class="h3 text-uppercase">- Available balance -</div>
                        <p class="available-balance"><span>&#8358;</span> 0.00</p>

                        <p>total saved so far &#8358; 0.00</p>
                    </div>
                </div>
                <div class="col-sm-4 stats">
                    <div class="text-center">
                        <div class="h3 text-uppercase">- Current Goal -</div>
                        <p class="available-balance"><span>&#8358;</span> 0.00</p>

                        <p>No goals yet</p>

                    </div>
                </div>
                <div class="col-sm-4 stats">
                    <div class="text-center">
                        <div class="h3 text-uppercase">- Next Milestone -</div>
                        <p class="available-balance">No milestone</p>

                        <p>0 milestones to go</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="dashboard-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="activity-empty-state">
                        <div class="text-center">
                            <div class="center-block">
                                <a href="#" class="new-plan-link js-overlay-open">
                                    <div>
                                        <i class="pe-7s-piggy"></i>

                                    </div>

                                    <p>Welcome to Kolowize. <br><br>Start saving, create a goal now</p>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
	
	<style>
	.dd{
		width:100%;
		height:100%;
		position:fixed;
		display:none;
		top:0;
		z-index:9999999999;
	}
	</style>
	<div class="dd">
	</div>
<?php
   /* echo $_SESSION['namefull'];
echo $_SESSION['email'];
echo $_SESSION['usrid'];
echo $_SESSION['type'];*/
include("../inc/savings.php");
?>


</main>
</body>
<script src="../js/jquery-2.2.2.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/preloader.js"></script>
<script src="../js/dashboard.js"></script>
<script src="../js/modal_savings.js"></script>
</html>
