<?php
require_once('function.php');
function checkifloggedin()
{
if(isset($_SESSION['usrid']) && ($_SESSION['type'] == 2 ))
{
redirect_to('dashboard');	
}
}

function confirmlogin()
{
if(!isset($_SESSION['usrid']))
{
redirect_to('../logout');	
}
else if(isset($_SESSION['usrid']) && ($_SESSION['type'] != 2 ))
{
redirect_to('../logout');	
}
}
function checksavings()
{
    global $msg;
    $connection = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
    if(!$connection)
    {
        die('Error establishing connection');
    }
    $stmt = mysqli_stmt_init($connection);
    $prep = mysqli_stmt_prepare($stmt,'select savingsid,userid,totalamount from savings where userid = ?  LIMIT 1');
    $bindparam = mysqli_stmt_bind_param($stmt,'s',$_SESSION['usrid'] );
    $exec = mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt,$savingsid,$userid,$totalamount);
    mysqli_stmt_store_result($stmt);
    $numrows = mysqli_stmt_num_rows($stmt);
    if($numrows == 1)
    {

       // $msg = 'found';
        redirect_to('dashboard');

    }
    if($numrows == 0)
    {
        $msg = 'new';
    }

}
?>