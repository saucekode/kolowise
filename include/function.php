<?php
/*if(!defined('RESTRICT'))
exit('No Direct Script Access Allowed');*/
session_start();
/*if(!isset($_SESSION['usrid']))
{
redirect_to('../logout/');
}*/
require_once('constants.php');
function redirect_to($location)
{

    exit(header("Location:{$location}"));

}


function findAdmin($username,$password)
{
    global $msg;
    $connection = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
    if(!$connection)
    {
        die('Error establishing connection');
    }
    $stmt = mysqli_stmt_init($connection);
    $prep = mysqli_stmt_prepare($stmt,'select firstname,lastname,email,userid,type from users where email = ? AND password = ? LIMIT 1');
    $password = hash('sha512',$password);
    $bindparam = mysqli_stmt_bind_param($stmt,'ss',$username,$password);
    $exec = mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt,$firstname,$lastname,$email,$id,$type);
    mysqli_stmt_store_result($stmt);
    $numrows = mysqli_stmt_num_rows($stmt);
    if($numrows == 1)
    {
        mysqli_stmt_fetch($stmt);
        $_SESSION['namefull'] = $lastname.' '.$firstname;
        $_SESSION['email'] = $email;
        $_SESSION['usrid'] = $id;
        $_SESSION['type'] = $type;
        $msg = 'found';

    }
    if($numrows == 0)
    {
        $msg = 'notfound';
    }

}

function signup($firstname,$lastname,$mobile_no,$password,$email)
{
    global $msg;
    $connection = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
    if(!$connection)
    {
        die('Error establishing connection');
    }
    $stmt = mysqli_stmt_init($connection);
    $prep = mysqli_stmt_prepare($stmt,'insert into users (firstname,lastname,mobile_no,email,password) values (?,?,?,?,?)');
    $password = hash('sha512',$password);
    $bindparam = mysqli_stmt_bind_param($stmt,'sssss',$firstname,$lastname,$mobile_no,$email,$password);
    $exec = mysqli_stmt_execute($stmt);
    $affectedrows = mysqli_stmt_affected_rows($stmt);
    if($affectedrows == 1)
    {


        $prep = mysqli_stmt_prepare($stmt,'select firstname,lastname,email,userid,type from users where mobile_no = ?  LIMIT 1');
        $bindparam = mysqli_stmt_bind_param($stmt,'s',$mobile_no);
        $exec = mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt,$firstname,$lastname,$email,$id,$type);
        mysqli_stmt_store_result($stmt);
        $numrows = mysqli_stmt_num_rows($stmt);
        if($numrows == 1)
        {
            mysqli_stmt_fetch($stmt);
            $_SESSION['namefull'] = $lastname.' '.$firstname;
            $_SESSION['email'] = $email;
            $_SESSION['usrid'] = $id;
            $_SESSION['type'] = $type;

            $msg = 'found';

        }
        $msg = 'found';

    }
}
function checkemail($email,$table)
{
    global $msg;
    $connection = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
    if(!$connection)
    {
        die('Error establishing connection');
    }
    $stmt = mysqli_stmt_init($connection);
    $prep = mysqli_stmt_prepare($stmt,'select email from '.$table.' where email=? LIMIT 1');
    $bindparam = mysqli_stmt_bind_param($stmt,'s',$email);
    $exec = mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt,$email);
    mysqli_stmt_store_result($stmt);
    $numrow = mysqli_stmt_num_rows($stmt);
    if($numrow == 1)
    {
        $msg = "emailfound";
    }
}

function checkmobile($mobile_no,$table)
{
    global $msg;
    $connection = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
    if(!$connection)
    {
        die('Error establishing connection');
    }
    $stmt = mysqli_stmt_init($connection);
    $prep = mysqli_stmt_prepare($stmt,'select mobile_no from '.$table.' where mobile_no=? LIMIT 1');
    $bindparam = mysqli_stmt_bind_param($stmt,'s',$mobile_no);
    $exec = mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt,$mobile);
    mysqli_stmt_store_result($stmt);
    $numrow = mysqli_stmt_num_rows($stmt);
    if($numrow == 1)
    {
        $msg = "mobilefound";
    }
}
function checkemail2($email,$table,$idfield,$id)
{
    global $msg;
    $connection = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
    if(!$connection)
    {
        die('Error establishing connection');
    }
    $stmt = mysqli_stmt_init($connection);
    $prep = mysqli_stmt_prepare($stmt,'select email from '.$table.' where email=? AND '.$idfield.'!=? LIMIT 1');
    $bindparam = mysqli_stmt_bind_param($stmt,'sd',$email,$id);
    $exec = mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt,$email);
    mysqli_stmt_store_result($stmt);
    $numrow = mysqli_stmt_num_rows($stmt);
    if($numrow == 1)
    {
        $msg = "emailfound";
    }
}
function checkusername2($email,$table,$idfield,$id)
{
    global $msg;
    $connection = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
    if(!$connection)
    {
        die('Error establishing connection');
    }
    $stmt = mysqli_stmt_init($connection);
    $prep = mysqli_stmt_prepare($stmt,'select username from '.$table.' where username=? AND '.$idfield.'!=? LIMIT 1');
    $bindparam = mysqli_stmt_bind_param($stmt,'sd',$email,$id);
    $exec = mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt,$email);
    mysqli_stmt_store_result($stmt);
    $numrow = mysqli_stmt_num_rows($stmt);
    if($numrow == 1)
    {
        $msg = "usernamefound";
    }
}
function editretailadmin($firstname,$lastname,$username,$password,$email,$id)
{
    global $msg;
    $connection = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
    if(!$connection)
    {
        die('Error establishing connection');
    }
    $stmt = mysqli_stmt_init($connection);
    if($password != "")
    {
        $prep = mysqli_stmt_prepare($stmt,'update retail_admin set  firstname=?,lastname=?,email=?,username=?,password=? where retail_admin_id=?');
        $password = hash('sha512',$password);
        $bindparam = mysqli_stmt_bind_param($stmt,'sssssd',$firstname,$lastname,$email,$username,$password,$id);
    }
    else if($password == "")
    {
        $prep = mysqli_stmt_prepare($stmt,'update retail_admin set  firstname=?,lastname=?,,email=?,username=? where retail_admin_id=?');
        $bindparam = mysqli_stmt_bind_param($stmt,'ssssd',$firstname,$lastname,$email,$username,$id);
    }
    $exec = mysqli_stmt_execute($stmt);
    $affectedrows = mysqli_stmt_affected_rows($stmt);
    if($affectedrows == 1)
    {
        $msg = 'Retail Admin Updated Successfully';
        insertSuperlog($_SESSION['namefull'].' edited a Retail admin ('.$firstname.' '.$lastname.')');
    }
    else
    {
        $msg = 'No changes was made';
    }
}


function dashboard()
{
global $purpose,$targetamount,$startdate,$amount,$next_pay_day, $total_amount;
$userid=$_SESSION['usrid'];
$connection = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
if(!$connection)
{
die('Error establishing connection');
}
$stmt = mysqli_stmt_init($connection);
$prep = mysqli_stmt_prepare($stmt,'SELECT savingsid,purpose,targetamount,datecreated FROM savings where userid = ? order by datecreated desc limit 1');
$bindparam = mysqli_stmt_bind_param($stmt,'d',$userid);
$exec = mysqli_stmt_execute($stmt);
mysqli_stmt_bind_result($stmt,$savingid,$purpose,$targetamount,$datecreated);
mysqli_stmt_store_result($stmt);
$numrows = mysqli_stmt_num_rows($stmt);
if($numrows == 1)
    {
        mysqli_stmt_fetch($stmt);
        $savingsid =   $savingid;
        $purpose= $purpose;
        $targetamount=$targetamount;
        $date= date_create($datecreated);
        $startdate=date_format($date, 'jS F Y');
        $paystatus=0;
$prep = mysqli_stmt_prepare($stmt,'SELECT amount,next_pay_day FROM cronjob where savings_id = ? AND pay_status=? order by datecreated asc limit 1');
$bindparam = mysqli_stmt_bind_param($stmt,'dd',$savingsid,$paystatus);
$exec = mysqli_stmt_execute($stmt);
mysqli_stmt_bind_result($stmt,$amount,$next_pay_days);
mysqli_stmt_store_result($stmt);
$numrows = mysqli_stmt_num_rows($stmt);
if($numrows == 1)
    {
     mysqli_stmt_fetch($stmt);
     $amount=$amount;
     $net= date_create($next_pay_days);
     $next_pay_day=date_format($net, 'jS F Y');
     $payment_status = 1;

     $prep = mysqli_stmt_prepare($stmt,'SELECT sum(amount) as amount FROM cronjob where savings_id = ? AND pay_status=? group by savings_id');
    $bindparam = mysqli_stmt_bind_param($stmt,'dd',$savingsid,$payment_status);
    $exec = mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt,$amount);
    mysqli_stmt_store_result($stmt);
    $numrows = mysqli_stmt_num_rows($stmt);

    if($numrows == 1)
    {
        mysqli_stmt_fetch($stmt);
        $total_amount = $amount;
    }
     
    }

}
}

function gettotalsave($savingsid)
  {
        $payment_status = 1;
        $connection = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        if(!$connection)
        {
            die('Error establishing connection');
        }
        $stmt = mysqli_stmt_init($connection);
     

     $prep = mysqli_stmt_prepare($stmt,'SELECT sum(amount) as amount FROM cronjob where savings_id = ? AND pay_status=? group by savings_id');
    $bindparam = mysqli_stmt_bind_param($stmt,'dd',$savingsid,$payment_status);
    $exec = mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt,$amounts);
    mysqli_stmt_store_result($stmt);
    $numrows = mysqli_stmt_num_rows($stmt);
        if($numrows == 1)
        {
            mysqli_stmt_fetch($stmt);
            $tamounts = $amounts;
            return $amounts;
        }
      //  return false;
    }


function getsavingsplan(){
    global   $savingsplan;
    $userid=$_SESSION['usrid'];
    // Create connection
$conn = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_NAME);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "select savingsid,purpose,targetAmount,status from savings where userid ='$userid' order by datecreated desc limit 2";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_array()) {

        if($row["status"] == 1){
            $realstatus = 'active'; 
        } else if($status == 0){
            $realstatus = 'inactive';   
        }

        $savingsplan .= '<a href="mysavings.php" class="dashboard-savings-summary">
            <div class="row">
                <div class="col-sm-8 col-xs-12">
                    <div class="summary-savings-name">
                        <p><i class="pe-7s-piggy"></i> '.$row["purpose"].'</p>
                    </div>

                </div>
                <div class="col-sm-4 col-xs-12 pull-right">
                    <div class="summary-savings-amount">
                        <span class="'.$realstatus.'">&#8358; '.number_format($row["targetAmount"]).'</span>
                        <span>available &#8358; '.number_format(gettotalsave($row["savingsid"])).'</span>
                        
                    </div>
                </div>
            </div>
        </a>';

    }


}else{
        $savingsplan.='no savingsplan';
    }

$conn->close();
}





function getsavingsplan2(){
    global   $savingsplan2;
    $userid=$_SESSION['usrid'];
    // Create connection
$conn = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_NAME);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "select * from savings where userid ='$userid' order by datecreated desc limit 4";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row

   // $savingsplan2 = $result->fetch_assoc();

    $savingsplan2 = '';
    
    while($row = $result->fetch_array()) {

        if($row["status"] == 1){
            $realstatus = 'active'; 
        } else if($status == 0){
            $realstatus = 'inactive';  
        }
        $date= date_create($row["datecreated"]);
        $startdate=date_format($date, 'jS F Y');
       
   $savingsplan2 .= '<div class="col-md-6">

   <div class="dashboard-savings-details">
                                            <div class="heading">

                                                <div class="row">

                                                    <div class="col-sm-9 col-xs-12" style="margin-bottom: 10px">
                                                        <img src="../img/img1.jpg" width="40px" height="40px">
                                                        <span class="savings-purpose">'.$row["purpose"].'</span>
                                                    </div>

                                                    <div class="col-sm-3 col-xs-12 pull-right">
                                                        <div class="custom-label '.$realstatus.' text-uppercase">
                                                            <i class="icon ion-ios-circle-outline"></i> '.$realstatus.'
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="body">

                                                <div class="row">

                                                    <div class="col-sm-4 text-center">
                                                        <div class="h3">&#8358; '.number_format($row["targetAmount"]).'</div>
                                                        <p>Amount to save</p>
                                                    </div>

                                                    <div class="col-sm-4 text-center">
                                                        <div class="h3 text-capitalize">'.$startdate.'</div>
                                                        <p>Started saving</p>
                                                    </div>

                                                    <div class="col-sm-4 text-center">
                                                        <div class="h3">&#8358; '.
                                                        number_format(gettotalsave($row["savingsid"]))
                                                        .
                                                        '</div>
                                                        <p>Amount saved</p>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                        </div>';


    }


}else{
       $savingsplan2.='no savingsplan';
    //$savingsplan2 = '';
    }

    //return $rows;

$conn->close();
}




function getmysavingsplan(){
    global   $savingsplan3;
    $userid=$_SESSION['usrid'];
    // Create connection
$conn = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_NAME);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "select * from savings where userid ='$userid' order by datecreated desc";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row

   // $savingsplan2 = $result->fetch_assoc();

    $savingsplan3 = '';
    
    while($row = $result->fetch_array()) {

        if($row["status"] == 1){
            $realstatus = 'active'; 
        } else if($status == 0){
            $realstatus = 'inactive';  
        }
        $date= date_create($row["datecreated"]);
        $startdate=date_format($date, 'jS F Y');
       
   $savingsplan3 .= '<div class="col-sm-3">
                    <div class="panel panel-default goal">
                        <div class="panel-heading">
                            <a class="goal-img-container js-side-modal-open">
                                <div class="goal-img"></div>
                            </a>

                        </div>
                        <div class="panel-body">
                            <div class="goal-thumbtext-container">
                                <div class="text-center">
                                    <div class="center-block">
                                        <div class="goal-title " href="#">'.$row["purpose"].'</div>

                                        <div class="custom-label  '.$realstatus.'  text-uppercase"><i
                                                class="icon ion-ios-circle-outline"></i>  '.$realstatus.' 
                                        </div>
                                    </div>
                                </div>
                                <div class="row info-list">
                                    <div class="col-xs-6 info-list-item">
                                        <span><i class="pe-7s-clock"></i> Started</span>
                                    </div>
                                    <div class="col-xs-6 text-right info-list-item">
                                        <span class="text-uppercase">'.$startdate.'</span>

                                    </div>
                                </div>
                                <div class="row info-list">
                                    <div class="col-xs-6 info-list-item">
                                        <span><i class="pe-7s-piggy"></i> Saved</span>
                                    </div>
                                    <div class="col-xs-6 text-right info-list-item">
                                        <span class="text-uppercase">&#8358; '.
                                                        number_format(gettotalsave($row["savingsid"]))
                                                        .
                                                        '</span>

                                    </div>
                                </div>
                                <div class="row info-list">
                                    <div class="col-xs-6 info-list-item">
                                        <span><i class="pe-7s-cash"></i> Amount</span>
                                    </div>
                                    <div class="col-xs-6 text-right info-list-item">
                                        <span class="text-uppercase">&#8358; '.number_format($row["targetAmount"]).'</span>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
';


    }



}else{
       $savingsplan3.='no savingsplan';
    //$savingsplan2 = '';
    }

    //return $rows;

$conn->close();
}






?>
