<?php
/**
 * Created by PhpStorm.
 * User: Afeez
 * Date: 2/8/2017
 * Time: 11:51 AM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/ionicons.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700" rel="stylesheet">
    <title>Reset Password Page</title>
</head>
<body>
<div class="preloader">
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div>
</div>
<header class="text-center auth-page-header">
    <div class="h1">
        <a href="index.html" class="text-capitalize app-logo">kolowize</a>
    </div>
</header>
<main>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="form-container">
                    <p class="text-center helper-text">It seems you forgot your Kolowize pin. Reset it Here</p>
                    <form class="auth-form">
                        <div class="form-group">
                            <input class="form-control text-capitalize" type="tel" id="phone_number"
                                   placeholder="Phone Number">
                        </div>
                        <div class="form-group">
                            <input class="form-control text-capitalize" type="password" id="old_pin"
                                   placeholder="old kolowize pin">
                        </div>
                        <div class="form-group">
                            <input class="form-control text-capitalize" type="password" id="new_pin"
                                   placeholder="new kolowize pin">
                        </div>
                        <input class="kw-btn big full text-uppercase" type="submit" value="reset pin">
                    </form>
                    <div class="login-btn-container">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <a class="kw-btn-transparent big text-uppercase" href="login.php">log in</a>

                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <a class="kw-btn-transparent big text-uppercase" href="register.php">sign up</a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="js/jquery-2.2.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/preloader.js"></script>
</body>
</html>
