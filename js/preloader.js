/**
 * Created by kola on 1/14/2017.
 */

$(function () {
    $(window).load(function () {
        $('.preloader').delay(350).fadeOut('slow');
        $('body').delay(350).css('overflow', 'visible');
    });
})