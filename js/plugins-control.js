/**
 * Created by kola on 1/20/2017.
 */
$(function() {

    var controller = new ScrollMagic.Controller();



    var navigationScene = new ScrollMagic.Scene({
        triggerElement: '.hero-headline',
        offset: 250
    });

    navigationScene.setClassToggle('.navigation', 'change-bg').addTo(controller);

    $('.section').each(function () {
        var sectionScene = new ScrollMagic.Scene({
            triggerElement: this,
            reverse: false
        });

        sectionScene
            .setClassToggle(this, 'fadeInUp')
            .addTo(controller);

    });
});