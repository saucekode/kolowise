<?php
/**
 * Created by PhpStorm.
 * User: Afeez
 * Date: 2/8/2017
 * Time: 3:32 PM
 */

require_once('../include/check.php'); confirmlogin();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0 width=device-width">
    <title>Dashboard</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/animate.min.css">
    <link rel="stylesheet" href="../css/ionicons.min.css">
    <link rel="stylesheet" href="../css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="../css/sweetalert.css">
    <link rel="stylesheet" href="../css/pikaday.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/dashboard.css">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Iceland" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top " id="dashboard-navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-"
                    aria-expanded="false" id="nav-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar" style="width: 15px"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" id="app-logo-dashboard" href="index.html">kolowize</a>
        </div>

        <div class="collapse navbar-collapse" id="navbar-collapse-">
            <ul class="nav navbar-nav" id="dashboard-main-nav">
                <li><a class="nav-links" href="dashboard_empty.html"><i class="pe-7s-graph1"></i> Dashboard</a></li>
                <li role="separator" class="mobile-link-divider"></li>
                <li><a class="nav-links" href="dashboard_my_savings.html"><i class="pe-7s-server"></i> My Savings</a>
                </li>
                <li role="separator" class="mobile-link-divider"></li>
                <li><a class="nav-links" href="#"><i class="pe-7s-gift"></i> My Rewards</a></li>
                <li role="separator" class="mobile-link-divider"></li>
                <li><a class="nav-links js-overlay-open" href="#"><i class="pe-7s-wallet"></i> New Savings Plan</a></li>
                <li role="separator" class="mobile-link-divider"></li>
                <li class="hidden-lg hidden-md hidden-sm"><a href="#" class="nav-links"><i class="pe-7s-config"></i>
                        Settings</a></li>
                <li role="separator" class="mobile-link-divider"></li>
                <li class="hidden-lg hidden-md hidden-sm"><a href="index.html" class="nav-links"><i
                            class="pe-7s-power"></i> Sign
                        Out</a></li>

            </ul>
            <ul class="nav navbar-nav navbar-right hidden-xs" id="dashboard-sec-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <i class="pe-7s-user"></i> Hi <?php echo ucwords($_SESSION['namefull']);?>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="dashboard_settings.html" class="sec-nav-links"><i class="pe-7s-config"></i>
                                Settings</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#" class="sec-nav-links"><i class="pe-7s-power"></i> Sign Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<main>
    <div class="activity-summary">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 stats">
                    <div class="text-center">
                        <div class="h3 text-uppercase">- Available balance -</div>
                        <p class="available-balance"><span>&#8358;</span> 0.00</p>

                        <p>total saved so far &#8358; 25,000</p>
                    </div>
                </div>
                <div class="col-sm-4 stats">
                    <div class="text-center">
                        <div class="h3 text-uppercase">- Current Goal -</div>
                        <p class="available-balance"><span>&#8358;</span> 25,000</p>

                        <a href="dashboard_my_savings.html" class="stats-btn">summer vacation to Paris</a>

                    </div>
                </div>
                <div class="col-sm-4 stats">
                    <div class="text-center">
                        <div class="custom-label active text-uppercase"><i
                                class="icon ion-ios-circle-outline"></i> Active
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <p id="stats-small-text"><i class="pe-7s-piggy"></i> Summer vacation to Paris</p>
                            </div>
                        </div>
                        <div class="row info-list">
                            <div class="col-xs-6 info-list-item">
                                <span><i class="pe-7s-clock"></i> Started</span>
                            </div>
                            <div class="col-xs-6 text-right info-list-item">
                                <span class="text-uppercase">jan 23, 2017</span>

                            </div>
                        </div>
                        <div class="row info-list">
                            <div class="col-xs-6 info-list-item">
                                <span><i class="pe-7s-date"></i> Next Deposit</span>
                            </div>
                            <div class="col-xs-6 text-right info-list-item">
                                <span class="text-uppercase">feb 23, 2017</span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="dashboard-active-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="dashboard-summary">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="h3">
                                    <span>My savings</span>
                                    <a href="dashboard_settings.html" class="settings-icon pull-right"><i
                                            class="icon ion-android-settings"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <a href="dashboard_my_savings.html" class="dashboard-savings-summary">
                                    <div class="row">
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="summary-savings-name">
                                                <p><i class="pe-7s-piggy"></i> Summer vacation to Paris</p>
                                            </div>

                                        </div>
                                        <div class="col-sm-4 col-xs-12 pull-right">
                                            <div class="summary-savings-amount">
                                                <span class="active">&#8358; 5000</span>
                                                <span>available &#8358; 0.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <a href="dashboard_my_savings.html" class="dashboard-savings-summary">
                                    <div class="row">
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="summary-savings-name">
                                                <p><i class="pe-7s-piggy"></i> A new car</p>
                                            </div>

                                        </div>
                                        <div class="col-sm-4 col-xs-12 pull-right">
                                            <div class="summary-savings-amount">
                                                <span class="inactive">&#8358; 5000</span>
                                                <span>available &#8358; 0.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="dashboard-summary">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="h3">
                                    <span>Bank Accounts</span>
                                    <a href="dashboard_settings.html" class="settings-icon pull-right"><i
                                            class="icon ion-android-settings"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <a href="dashboard_settings.html" class="bank-details-summary">
                                    <div class="row">
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="summary-bank-name">
                                                <p><i class="icon ion-record active"></i> Access Bank</p>
                                            </div>

                                        </div>
                                        <div class="col-sm-4 col-xs-12 pull-right">
                                            <div class="summary-account-number">
                                                <p>2147483647</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <a href="dashboard_settings.html" class="bank-details-summary">
                                    <div class="row">
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="summary-bank-name">
                                                <p><i class="icon ion-record inactive"></i> Diamond Bank</p>
                                            </div>

                                        </div>
                                        <div class="col-sm-4 col-xs-12 pull-right">
                                            <div class="summary-account-number">
                                                <p>2147483647</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="dashboard-summary">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="h3">
                                    <span>My Rewards</span>
                                    <a href="dashboard_settings.html" class="settings-icon pull-right"><i
                                            class="icon ion-android-settings"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <a href="dashboard_my_savings.html" class="dashboard-savings-summary">
                                    <div class="row">
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="summary-savings-name">
                                                <p><i class="pe-7s-piggy"></i> Summer vacation to Paris</p>
                                            </div>

                                        </div>
                                        <div class="col-sm-4 col-xs-12 pull-right">
                                            <div class="summary-savings-amount">
                                                <span class="active">&#8358; 5000</span>
                                                <span>available &#8358; 0.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <a href="dashboard_my_savings.html" class="dashboard-savings-summary">
                                    <div class="row">
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="summary-savings-name">
                                                <p><i class="pe-7s-piggy"></i> A new car</p>
                                            </div>

                                        </div>
                                        <div class="col-sm-4 col-xs-12 pull-right">
                                            <div class="summary-savings-amount">
                                                <span class="inactive">&#8358; 5000</span>
                                                <span>available &#8358; 0.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div class="dashboard-details">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="savings-summary-details">
                                    <div class="row">
                                        <div class="col-sm-4 col-xs-12">
                                            <form style="margin-bottom: 10px">
                                                <div class="form-group inner-addon">
                                                    <div>
                                                        <i class="pe-7s-angle-down form-icon"></i>
                                                    </div>
                                                    <input class="form-control" id="datepicker"
                                                           placeholder="Select date">
                                                </div>
                                            </form>

                                        </div>
                                        <div class="col-sm-8 col-xs-12 pull-right">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="monthly-savings-summary text-center">
                                                        <div class="h3">&#8358; 5000</div>
                                                        <p>Average monthly savings</p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <a href="dashboard_my_savings.html"
                                                       class="total-savings-summary text-center">
                                                        <div class="h3">2</div>
                                                        <p>Total savings plans</p>
                                                    </a>
                                                </div>
                                                <div class="col-sm-4">
                                                    <a href="#" class="total-rewards-summary text-center">
                                                        <div class="h3">2</div>
                                                        <p>Total rewards earned</p>
                                                    </a>

                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="dashboard-savings-details">
                                            <div class="heading">

                                                <div class="row">
                                                    <div class="col-sm-9 col-xs-12" style="margin-bottom: 10px">
                                                        <img src="img/img1.jpg" width="40px" height="40px">
                                                        <span class="savings-purpose">Summer vacation to Paris</span>
                                                    </div>
                                                    <div class="col-sm-3 col-xs-12 pull-right">
                                                        <div class="custom-label active text-uppercase"><i
                                                                class="icon ion-ios-circle-outline"></i> Active
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="body">
                                                <div class="row">
                                                    <div class="col-sm-4 text-center">
                                                        <div class="h3">&#8358; 125,000</div>
                                                        <p>Amount to save</p>
                                                    </div>
                                                    <div class="col-sm-4 text-center">
                                                        <div class="h3 text-capitalize">jan 23, 2017</div>
                                                        <p>Started saving</p>
                                                    </div>
                                                    <div class="col-sm-4 text-center">
                                                        <div class="h3">&#8358; 125,000</div>
                                                        <p>Amount saved</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="dashboard-savings-details">
                                            <div class="heading">

                                                <div class="row">
                                                    <div class="col-sm-9 col-xs-12" style="margin-bottom: 10px">
                                                        <img src="img/img1.jpg" width="40px" height="40px">
                                                        <span class="savings-purpose">A new car</span>
                                                    </div>
                                                    <div class="col-sm-3 col-xs-12 pull-right">
                                                        <div class="custom-label inactive text-uppercase"><i
                                                                class="icon ion-ios-circle-outline"></i> Inactive
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="body">
                                                <div class="row">
                                                    <div class="col-sm-4 text-center">
                                                        <div class="h3">&#8358; 125,000</div>
                                                        <p>Amount to save</p>
                                                    </div>
                                                    <div class="col-sm-4 text-center">
                                                        <div class="h3 text-capitalize">jan 23, 2017</div>
                                                        <p>Started saving</p>
                                                    </div>
                                                    <div class="col-sm-4 text-center">
                                                        <div class="h3">&#8358; 125,000</div>
                                                        <p>Amount saved</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="dashboard-savings-details">
                                            <div class="heading">

                                                <div class="row">
                                                    <div class="col-sm-9 col-xs-12" style="margin-bottom: 10px">
                                                        <img src="img/img1.jpg" width="40px" height="40px">
                                                        <span class="savings-purpose">Summer vacation to Paris</span>
                                                    </div>
                                                    <div class="col-sm-3 col-xs-12 pull-right">
                                                        <div class="custom-label active text-uppercase"><i
                                                                class="icon ion-ios-circle-outline"></i> Active
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="body">
                                                <div class="row">
                                                    <div class="col-sm-4 text-center">
                                                        <div class="h3">&#8358; 125,000</div>
                                                        <p>Amount to save</p>
                                                    </div>
                                                    <div class="col-sm-4 text-center">
                                                        <div class="h3 text-capitalize">jan 23, 2017</div>
                                                        <p>Started saving</p>
                                                    </div>
                                                    <div class="col-sm-4 text-center">
                                                        <div class="h3">&#8358; 125,000</div>
                                                        <p>Amount saved</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="dashboard-savings-details">
                                            <div class="heading">

                                                <div class="row">
                                                    <div class="col-sm-9 col-xs-12" style="margin-bottom: 10px">
                                                        <img src="img/img1.jpg" width="40px" height="40px">
                                                        <span class="savings-purpose">A new car</span>
                                                    </div>
                                                    <div class="col-sm-3 col-xs-12 pull-right">
                                                        <div class="custom-label inactive text-uppercase"><i
                                                                class="icon ion-ios-circle-outline"></i> Inactive
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="body">
                                                <div class="row">
                                                    <div class="col-sm-4 text-center">
                                                        <div class="h3">&#8358; 125,000</div>
                                                        <p>Amount to save</p>
                                                    </div>
                                                    <div class="col-sm-4 text-center">
                                                        <div class="h3 text-capitalize">jan 23, 2017</div>
                                                        <p>Started saving</p>
                                                    </div>
                                                    <div class="col-sm-4 text-center">
                                                        <div class="h3">&#8358; 125,000</div>
                                                        <p>Amount saved</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <a class="view-all-btn" href="dashboard_my_savings.html">View All Savings</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


   <?php
 include("../inc/savings.php");
   ?>
</main>
</body>
<script src="../js/jquery-2.2.2.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>
<script src="../js/pikaday.js"></script>
<script src="../js/preloader.js"></script>
<script src="../js/dashboard.js"></script>
<script src="../js/modal_savings.js"></script>
</html>
