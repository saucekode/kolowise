/**
 * Created by kola on 1/28/2017.
 */

$(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $('.js-overlay-open').click(openOverlay);
    $('.js-close-btn').click(closeOverlay);

    function openOverlay() {
        $("#overlay").css('height', '100%');
        setTimeout(function () {
            $('.js-sk-folding-cube').css({display: 'none'});

            $('.new-plan-content').css({display: 'block'});
        }, 3000);
    }

    function closeOverlay() {
        $("#overlay").css('height', '0');
    }

    var current_fs, prev_fs, next_fs,
        timeline_select = $('#timeline-select'),
        custom_timeline = $('.js-no-timeline'),
        side_modal = $('#side-overlay'),
        side_overlay_trigger = $('.js-side-modal-open'),
        close_side_modal = $('.js-close-side-modal'),
        withdraw_section = $('.withdraw-form-container'),
        plan_details_section = $('.plan-details'),
        back_btn = $('.js-back-btn'),
        withdraw_options = $('#withdraw-option'),
        custom_withdraw_amount = $('#custom-withdraw-amount'),
        confirm_witdraw_btn = $('.js-withdraw-confirm');

    $(document).on('click', '.js-next', function () {
        current_fs = $(this).parents('.js-fieldset');
        next_fs = $(this).parents('.js-fieldset').next();

        current_fs.hide();
        next_fs.show();
    });
    $(document).on('click', '.js-previous', function () {
        current_fs = $(this).parents('.js-fieldset');
        prev_fs = $(this).parents('.js-fieldset').prev();

        current_fs.hide();
        prev_fs.show();
    });

    side_overlay_trigger.on('click', function () {
        side_modal.toggleClass('show-side-modal');

    });

    close_side_modal.click(function () {
        side_modal.removeClass('show-side-modal');
    });

    timeline_select.change(function () {
        if(timeline_select.val() == 1) {
            custom_timeline.css({
                display: 'block',
                opacity: 1,
                transform: 'translateY(0)'
            });
        }
        else {
            custom_timeline.css({
                display: 'none',
                opacity: 0,
                transform: 'translateY(50)'
            });

        }

    });

    withdraw_section.hide();
    back_btn.hide();

    $('.js-withdraw-btn').click(function () {
        withdraw_section.animate({opacity: 1}, 'slow').show();
        back_btn.show();
        plan_details_section.hide();
    });

    back_btn.click(function () {
        withdraw_section.animate({opacity: 0}, 'slow').hide();
        back_btn.hide();
        plan_details_section.show();

    });

    custom_withdraw_amount.hide();
    withdraw_options.change(function () {
        if(withdraw_options.val() == 2) {
            custom_withdraw_amount.animate({
                opacity: 1
            }, 'slow').show();
        }
        else {
            custom_withdraw_amount.animate({
                opacity: 0
            }, 'slow').hide();

        }
    });
    
    confirm_witdraw_btn.click(function () {
        swal({
                title: "Are you sure you want to withdraw this amount?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#00C853",
                confirmButtonText: "Yes, withdraw it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm) {
                    swal("Amount withdrawn!", "Your money for this plan has been withdrawn.", "success");
                } else {
                    swal("Cancelled", "Your money is still safe :)");
                }
            });
    });

    var datePicker = new Pikaday({ field: document.getElementById('datepicker') });


});