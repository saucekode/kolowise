<?php
/**
 * Created by PhpStorm.
 * User: Afeez
 * Date: 1/17/2017
 * Time: 1:30 PM
 */
echo "continue";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="https://da0mzqp3tlely.cloudfront.net/js/jquery.min.js" type="text/javascript" ></script>
    <script src="https://da0mzqp3tlely.cloudfront.net/js/jquery.livequery_new_compress.js" type="text/javascript" ></script>
    <script src="https://da0mzqp3tlely.cloudfront.net/js/chosen.jquery.js"></script>
    <link href="https://da0mzqp3tlely.cloudfront.net/css/bootstrap.css" rel="stylesheet">
    <link href="https://da0mzqp3tlely.cloudfront.net/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="https://da0mzqp3tlely.cloudfront.net/css/datepicker.css" rel="stylesheet">

</head>
<body>
<div class="container">
    <div class="row">
        <form role="form" class="col-md-9 go-right">
            <h2>Create a savings plan</h2>
            <p>To see how it works, you clink in a input field.</p>
            <div class="form-group">
                <label><b>Frequency</b> - How frequently do you want to deposit money?</label>
                <select id="frequency" class="form-control" required>
                    <option>--Select--</option>
                    <option value="daily">Daily</option>
                    <option value="weekly">Weekly</option>
                    <option value="monthly">Monthly</option>
                </select>

            </div>

            <div id="formdetails"></div>


            <div class="form-group">
                <input class="btn btn-primary btn-md" type="submit" value="Submit" id="plan_submit">
            </div>
        </form>

    </div>
</div>





</body>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script>
    $( "#frequency" ).change(function() {
        var selected_val = $( "#frequency option:selected" ).val();
        if(selected_val !== ""){
            $("#formdetails").load(selected_val+".php");
            $("#plan_submit").removeAttr("disabled");
        } else {
            $("#formdetails").html("");
            $("#plan_submit").attr("disabled","disabled");
        }
    });
</script>

</body>
</html>

