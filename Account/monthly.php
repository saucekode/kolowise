<?php
/**
 * Created by PhpStorm.
 * User: Afeez
 * Date: 2/2/2017
 * Time: 2:46 PM
 */
?>
<div class="main_content form_holder">

        <span class="prefreqhtml"><label><b>Choose the day of the month:</b></label>
        <select title="Choose the Day" name="monthday" class="monthday form-control" required="">

        	<option value="1">1st</option>

        	<option value="2">2nd</option>

        	<option value="3">3rd</option>

        	<option value="4">4th</option>

        	<option value="5">5th</option>

        	<option value="6">6th</option>

        	<option value="7">7th</option>

        	<option value="8">8th</option>

        	<option value="9">9th</option>

        	<option value="10">10th</option>

        	<option value="11">11th</option>

        	<option value="12">12th</option>

        	<option value="13">13th</option>

        	<option value="14">14th</option>

        	<option value="15">15th</option>

        	<option value="16">16th</option>

        	<option value="17">17th</option>

        	<option value="18">18th</option>

        	<option value="19">19th</option>

        	<option value="20">20th</option>

        	<option value="21">21st</option>

        	<option value="22">22nd</option>

        	<option value="23">23rd</option>

        	<option value="24">24th</option>

        	<option value="25">25th</option>

        	<option value="26">26th</option>

        	<option value="27">27th</option>

        	<option value="28">28th</option>
        </select>
        <br>
        <br>
        <input type="hidden" name="weekday" class="weekday" value="">
<label><b>Choose the time of the Day:</b></label>
        <select title="Choose the Time" name="timeday" class="timeday form-control" required="">
        	<option value="21600" selected="">06:00 am</option>
        	<option value="14400">04:00 am</option>
        	<option value="16200">04:30 am</option>
        	<option value="18000">05:00 am</option>
        	<option value="19800">05:30 am</option>
        	<option value="21600">06:00 am</option>
        	<option value="23400">06:30 am</option>
        	<option value="25200">07:00 am</option>
        	<option value="27000">07:30 am</option>
        	<option value="28800">08:00 am</option>
        	<option value="30600">08:30 am</option>
        	<option value="32400">09:00 am</option>
        	<option value="34200">09:30 am</option>
        	<option value="36000">10:00 am</option>
        	<option value="37800">10:30 am</option>
        	<option value="39600">11:00 am</option>
        	<option value="41400">11:30 am</option>
        	<option value="43200">12:00 pm</option>
        	<option value="45000">12:30 pm</option>
        	<option value="46800">01:00 pm</option>
        	<option value="48600">01:30 pm</option>
        	<option value="50400">02:00 pm</option>
        	<option value="52200">02:30 pm</option>
        	<option value="54000">03:00 pm</option>
        	<option value="55800">03:30 pm</option>
        	<option value="57600">04:00 pm</option>
        	<option value="59400">04:30 pm</option>
        	<option value="61200">05:00 pm</option>
        	<option value="63000">05:30 pm</option>
        	<option value="64800">06:00 pm</option>
        	<option value="66600">06:30 pm</option>
        	<option value="68400">07:00 pm</option>
        	<option value="70200">07:30 pm</option>
        	<option value="72000">08:00 pm</option>
        	<option value="73800">08:30 pm</option>
        	<option value="75600">09:00 pm</option>
        	<option value="77400">09:30 pm</option>
        	<option value="79200">10:00 pm</option>
        	<option value="81000">10:30 pm</option>
        	<option value="82800">11:00 pm</option>
        	<option value="84600">11:30 pm</option>
        </select>
        <br>
        <br>
</span>
        <!--<input type="hidden" name="frequency" id="frequency" value="daily">-->
        <span class="preamounthtml" style="display: inline;">
        <label><b>My <span class="freq">monthly</span> deposit</b> - How much can you afford to deposit <span class="freq">monthly</span>? <span class="limitdesc"> (between ₦5000 - ₦100000)</span></label>
        <input type="number" name="amount" id="amount" class="form-control" autocomplete="off">
        <span class="help-block"><small><i class="icon-flag"></i> This amount will be consistently saved into your PiggyBank.ng account <span class="freq">monthly</span>. </small>
        </span>
        <span class="prehtml">
        <br>
        <br>
        <input type="button" class="btn btn-info thecon" value="Continue">
        </span>
        <br>
        <br>
        <span class="posthtml"></span>
        <span class="finalbutton" style="display: none;"></span>
        <br>
<script src="https://js.paystack.co/v1/inline.js"></script>
<script type="text/javascript">
function generate(val){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < val; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}


function maxlimit(val){
    if(val == 'daily'){
        var lim = 5000;
    }else if(val == 'weekly'){
        var lim = 25000;
    }else if(val == 'monthly'){
        var lim = 100000;
    }
    return lim;
}

function minlimit(val){
    if(val == 'daily'){
        var lim = 50;
    }else if(val == 'weekly'){
        var lim = 1000;
    }else if(val == 'monthly'){
        var lim = 5000;
    }
    return lim;
}


function frequency(frequency){

    if(frequency){

        $.ajax({
			type: "POST",
			url: "https://www.piggybank.ng/startsavingajax",
			data: "calltype=frequency&frequency="+frequency,
			cache: false,
			beforeSend: function(html){
            $('.prefreqhtml').html('<i class="icon-refresh"></i> ... please wait<br /><br />');
            $('.preamounthtml').hide();
            $('.finalbutton').hide();
        },
			success: function(html){
            $('.prefreqhtml').html(html);
            $('.preamounthtml').show();
            $('.freq').html(frequency);
            $('.limitdesc').html(' (between ₦'+minlimit(frequency)+' - ₦'+maxlimit(frequency)+')');
            $('.finalbutton').hide();
        }
		});


	}else{
        $('.preamounthtml').hide();
        $('.finalbutton').hide();
    }

}

$("#confirmplan").livequery("click", function(e){
    $this = $(this);
    var cc = $this.is(':checked');
    if(cc){
        $("#payWithPaystack").attr('class', 'btn btn-success');
        $("#payWithPaystack").prop('disabled', false);
    }else{
        $("#payWithPaystack").attr('class', 'btn btn-danger');
        $("#payWithPaystack").prop('disabled', true);
    }
});

$("#payWithPaystack").livequery("click", function(e){
    $this = $(this);
    $('#thesample').hide();
    var oval = $($this).html();
    $($this).html('...please wait');
    $($this).prop('disabled', true);
    $('#confirmplan').prop('disabled', true);
    var value = $('#finalamount').val()*100;
    // var theref = $('#finalref').val();
    var theref = 'pb8bcc'+generate(6);
    var handler = PaystackPop.setup({
          key: 'pk_live_4d4f15217f2f6b0c93bec5912685f9d5edd2a752',
          email: 'awotundun@gmail.com',
          amount: value,
          ref: theref,
          callback: function(response) {
        // alert('Payment was successful. Please wait to be redirected. Transaction reference is ' + response.trxref);
        $.ajax({
			type: "POST",
			url: "https://www.piggybank.ng/startsavingajax",
			data: "inisave=startsaving&paystack-trxref="+response.trxref,
			cache: false,
			beforeSend: function(html){
            $($this).prop('disabled', false);
            $($this).html('please wait ... initiating savings');
            $($this).attr('class', 'btn');
            $($this).attr('id', '');
            $($this).prop('disabled', true);
        },
			success: function(html){
            alert('Nice, your savings has been initiated successfully. Click OK, let\'s show what you have saved today already.');
            $($this).prop('disabled', false);
            $($this).html('...redirecting');
            location.href='https://www.piggybank.ng/account';
        }
	     });
          },
          onClose: function() {
        $($this).html(oval);
        $($this).prop('disabled', false);
        $('#confirmplan').prop('disabled', false);
        $('#thesample').show();
    }
        });
        handler.openIframe();
});


$("#frequency").livequery("change", function(e){
    $this = $(this);
    var f = $this.val();
    frequency(f);
});


//frequency('daily');


$("#amount").livequery("keyup", function(e){
    $this = $(this);
    var f = $('#frequency').val();
    var amount = $this.val();
    if(amount > maxlimit(f)){
        amount = maxlimit(f);
        $this.val(amount);
    }
    var charge = Math.round(amount * (0/100));
    $('.charge').html('₦'+charge);
});


function showtarget(){
    var f = $('#frequency').val();
    var a = $("#amount").val();
    var td = $('.timeday').val();
    var wd = $('.weekday').val();
    var md = $('.monthday').val();
    if((a >= minlimit(f)) && a <= maxlimit(f) && (f != '') && (td != '')){
        $.ajax({
			type: "POST",
			url: "https://www.piggybank.ng/startsavingajax",
			data: "inisave=1&amount="+a+"&frequency="+f+"&timeday="+td+"&weekday="+wd+"&monthday="+md,
			cache: false,
			beforeSend: function(html){
            $('.posthtml').html('<i class="icon-refresh"></i> ... please wait, calculating');
            $('.thecon').val('... please wait, calculating');
        },
			success: function(html){
            $('.prehtml').hide();
            $('.posthtml').html(html);
            $('#target').focus();
        }
		});
	}else if(a > maxlimit(f)){
        alert('Amount to save must be less than ₦'+maxlimit(f)+' '+f+'.');
        $("#amount").focus();
    }else{
        alert('Please enter an amount you would like to save. Amount to save must be at least ₦'+minlimit(f)+' '+f+'.');
        $("#amount").focus();
    }
    $('.finalbutton').hide();
}





$(".thecon").livequery("click", function(e){
    showtarget();
});


$("#amount").livequery("change", function(e){
    showtarget();
});


$("#target").livequery("change", function(e){
    $this = $(this);
    var f = $('#frequency').val();
    var a = $('#amount').val();
    var t = $this.val();
    var td = $('.timeday').val();  alert(td);
    var wd = $('.weekday').val();
    var md = $('.monthday').val();
    if((a >= minlimit(f)) && a <= maxlimit(f) && (f != '') && (t > 100) && (td != '')){
        $('.finalbutton').show();
        $.ajax({
		type: "POST",
		url: "https://www.piggybank.ng/startsavingajax",
		data: "inisave=saveplan&amount="+a+"&frequency="+f+"&target="+t+"&timeday="+td+"&weekday="+wd+"&monthday="+md,
		cache: false,
		beforeSend: function(html){
            $('.finalbutton').html('<i class="icon-refresh"></i> ... please wait, processing');
        },

		success: function(html){
            $('.finalbutton').html(html);
            $('#fbutt').focus();
        }
		});
	}else if(a > maxlimit(f)){
        alert('Please ensure that all these fields are correctly filled before you proceed. Amount to save must be less than ₦'+maxlimit(f)+' '+f+'.');
        $this.focus();
    }else{
        alert('Please ensure that all these fields are correctly filled before you proceed. Amount to save must be more than ₦'+minlimit(f)+'');
        $this.focus();
        $('.finalbutton').hide();
    }
});


</script>
</span></div>