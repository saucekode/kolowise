<?php
/**
 * Created by PhpStorm.
 * User: Afeez
 * Date: 2/8/2017
 * Time: 11:18 AM
 */


require_once('include/function.php');
function checkifloggedin()
{
if(isset($_SESSION['usrid']) && ($_SESSION['type'] == 2 ))
{
redirect_to('dashboard');   
}
}

function confirmlogin()
{
if(!isset($_SESSION['usrid']))
{
//redirect_to('login.php');   
}
else if(isset($_SESSION['usrid']) && ($_SESSION['type'] != 2 ))
{
//redirect_to('login.php');   
}
}
 confirmlogin();
include_once ("inc/header.php");
?>

<body>
<div class="preloader">
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div>
</div>
<header class="text-center auth-page-header">
    <div class="h1">
        <a href="index.php" class="text-capitalize app-logo">kolowize</a>
    </div>
</header>

<main>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="form-container">
                    <p class="text-center helper-text">Please login with your registered Kolowize phone number and 4 digit PIN.</p>

                        <form class="auth-form login-form" action="" method="post">
                            <div id="displaymsg"></div>
                        <div class="form-group">
                            <input class="form-control " type="email" id="username"
                                   placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input class="form-control " type="password" id="password"
                                   placeholder="kolowize pin">
                        </div>
                        <button class="kw-btn big full text-lowercase" id="l_pl" type="submit">Log In</button>
                    </form>
                    <div class="login-btn-container text-center">
                        <a class="kw-btn-transparent big text-lowercase" href="register.php">sign up</a>
                    </div>

                    <div class="reset-pin-container text-center">
                        <a class="kw-link kw-link-grey" href="forget.php">Forgot Your Kolowize Pin?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="js/jquery-2.2.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/preloader.js"></script>
<script src="js/op/login.js"></script>
</body>
</html>
