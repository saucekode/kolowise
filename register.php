<?php
/**
 * Created by PhpStorm.
 * User: Afeez
 * Date: 1/17/2017
 * Time: 10:23 AM
 */
include_once ("inc/header.php");
?>

<body>
<div class="preloader">
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div>
</div>
<header class="text-center auth-page-header">
    <div class="h1">
        <a href="index.php" class="text-capitalize app-logo">kolowize</a>
    </div>
</header>
<main>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="form-container">
                    <p class="text-center helper-text">Please fill the form below to get started.</p>

                        <form action="" id="" class=" auth-form form-horizontal">

                            <div id="displaymsg"></div>
                        <div class="form-group">
                            <input class="form-control " type="text" name="first_name" id="first_name"
                                   placeholder="First Name">
                        </div>
                        <div class="form-group">
                            <input class="form-control " type="text" name="last_name" id="last_name" 
                                   placeholder="Last Name">
                        </div>
                        <div class="form-group">
                            <input class="form-control " type="text" name="mobile_no" id="mobile_no" placeholder="Mobile No">
                        </div>
                        <div class="form-group">
                            <input class="form-control " type="email" id="email" name="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="password" name="password" id="password""
                                   placeholder="kolowize pin">
                        </div>
                        <label class="checkbox">
                            <span class="input-helper checkbox">
                                <input type="checkbox">
                                <span></span>
                                <em class="inline">I have read and agree to the <a class="kw-link kw-link-green" href="#">Terms</a></em>
                            </span>
                        </label>
                        <input class="kw-btn big full text-uppercase" type="submit" value="sign up">
                    </form>
                    <div class="login-btn-container text-center">
                        <a class="kw-btn-transparent big text-uppercase" href="login.php">log in</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="js/jquery-2.2.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/preloader.js"></script>
<script src="js/op/signup.js"></script>
</body>
</html>