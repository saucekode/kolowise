-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2017 at 03:25 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kolowise`
--

-- --------------------------------------------------------

--
-- Table structure for table `savings`
--

CREATE TABLE IF NOT EXISTS `savings` (
  `savingsid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `totalamount` varchar(50) NOT NULL,
  `datecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`savingsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(500) NOT NULL,
  `lastname` varchar(500) NOT NULL,
  `mobile_no` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(500) NOT NULL,
  `userpic` varchar(500) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '2',
  `datecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `firstname`, `lastname`, `mobile_no`, `email`, `username`, `password`, `userpic`, `type`, `datecreated`) VALUES
(2, 'Afeez', 'Awotundun', '08185466356', 'awotundun@gmail.com', '', '559abe5e105f55a91ff0a14c1ac40e38d7d239ffda698c2b2d3773569d2fdbf3e73785d673d305a13de41acead367606a4eb292587441485a5b49f8c35a96e1e', NULL, 2, '2017-01-17 12:31:37'),
(3, 'Tolu', 'Ade', '08122907682', 'abeltolu@gmail.com', '', '4b8123c456de3f142546813500be52af35031bd79af6eb1a8d77222860f3fc135cb7235daf73fa7655fda551dfccf53e562a47534b3d74a6fce57f164a06567d', NULL, 2, '2017-01-17 12:52:25'),
(4, 'Afeez', 'Awotundun', '081854663560', 'awotundun9@gmail.com', '', '559abe5e105f55a91ff0a14c1ac40e38d7d239ffda698c2b2d3773569d2fdbf3e73785d673d305a13de41acead367606a4eb292587441485a5b49f8c35a96e1e', NULL, 2, '2017-02-02 10:40:04'),
(5, 'dele', 'Ali', '09030397909', 'awotundunola@gmail.com', '', '559abe5e105f55a91ff0a14c1ac40e38d7d239ffda698c2b2d3773569d2fdbf3e73785d673d305a13de41acead367606a4eb292587441485a5b49f8c35a96e1e', NULL, 2, '2017-02-08 13:50:29');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
