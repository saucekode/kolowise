<?php
/**
 * Created by PhpStorm.
 * User: Afeez
 * Date: 2/14/2017
 * Time: 11:08 AM
 */

require_once('../include/check.php'); confirmlogin();


set_time_limit(0);

$userid=$_SESSION['usrid'];

$saving_amount = $_SESSION['saving_amount'];
$selected_frequency = $_SESSION['selected_frequency'];

$pur = $_SESSION['saving_for'];
$amt = $_SESSION['saving_towards'];
$time = $_SESSION['saving_timeline'];


$url = 'https://api.paystack.co/transaction/verify/'.$_REQUEST['reference'];

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$live_key = "sk_live_86e8dc6e895b21d6393c8ade8ee0771b475a736c";

$headers = array();
$headers[] = 'Authorization: Bearer '.$live_key;
$headers[] = 'Content-Type: application/json;';

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);

curl_close($ch);

$dec_result = json_decode($result);
$transact=$dec_result->data->reference;
$transactdetails=$dec_result->data->authorization->authorization_code;
if($dec_result->data->status == 'success'){

    //do something because the payment was successful
      // Create connection
	$conn = new mysqli(DB_HOST,DB_USER, DB_PASS, DB_NAME);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}

    $newquery = "INSERT INTO savings(userid,purpose,targetAmount,targetTime,paidamount,frequency, auth_code) VALUES ('$userid','$pur','$amt','$time','$saving_amount','$selected_frequency', '$transactdetails')";
  if ($conn->query($newquery) === TRUE) {

    //create the cron job
    $search_savings_query = "SELECT * FROM savings where userid = '$userid' order by datecreated desc limit 1";
    $savings_result = $conn->query($search_savings_query);
    if($savings_result->num_rows > 0){
        while ($row = $savings_result->fetch_assoc()){
            $id = $row['savingsid'];

            $todays_date = date('Y-m-d');
            $date = strtotime(date("Y-m-d", strtotime($todays_date)) . " +{$time} month"); 
            $new_date = date("Y-m-d", $date);
            $begin = new DateTime($todays_date);
            $end = new DateTime($new_date);
            $end = $end->modify( '+1 day' );

            if($selected_frequency == 'daily'){
                $interval_freq = 'P1D';
            }
            else if($selected_frequency == 'weekly'){
                $interval_freq = 'P1W';
            } else {
               $interval_freq = 'P1M'; 
            }

            $interval = new DateInterval($interval_freq);
            $daterange = new DatePeriod($begin, $interval, $end);
            
            foreach($daterange as $date){
                $savings_id = $id;
                $amount = $saving_amount;
                $next_pay_day = $date->format("Y-m-d");
                if(date("Y-m-d") == $next_pay_day){
                    $processed = 1;
                    $pay_status = 1;
                } else {
                    $processed = 0;
                    $pay_status = 0;
                }
                

                $sql = "INSERT into cronjob (savings_id, amount, next_pay_day, processed, pay_status) VALUES ('$savings_id', '$amount', '$next_pay_day', '$processed', $pay_status)";
                $conn->query($sql);
            }

        }
    }

        $responses='successful';
        $query3 = "UPDATE payment SET response = '$responses' WHERE transactionid = '$transact'";
        $conn->query($query3);
 
        header("Location: ../dashboard");

	}
else {
        $errTyp = "danger";
        $errMSG = "Something went wrong, try again later...";
    }






} else {
    $responsef="failed";
    $query = "UPDATE transaction SET response = '$responsef' WHERE transactionid = '$transact'";
    $conn->query($query);

    //do something cos the payment was not successful
    header("Location: paymentFailed.php");
}

?>












