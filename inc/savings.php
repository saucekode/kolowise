<?php
/**
 * Created by PhpStorm.
 * User: Afeez
 * Date: 2/8/2017
 * Time: 4:02 PM
 */
?>
<div class="new-plan-page" id="overlay" style="overflow: hidden;">
    <a href="javascript:void(0)" class="close-btn js-close-btn"><i class="pe-7s-close"></i></a>

    <div class="container">
        <div class="sk-folding-cube js-sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
        <div class="new-plan-content text-center">
            <div class="center-block">
                <form class="goal-form">
                    <fieldset class="js-fieldset">
                        <div class="h2">Start saving towards your goal now</div>
                        <div class="large-icon-container">
                            <i class="pe-7s-car"></i>

                            <div class="h3">Purpose</div>
                        </div>
                        <div class="form-group inner-addon">
                            <div>
                                <i class="icon ion-ios-pricetag-outline form-icon" id="purpose-icon"></i>
                            </div>
                            <input type="text" class="form-control"
                                   placeholder="Saving Purpose (i.e a new phone)" onKeyUp="check_svn_pur(this.value)" required="required" id="saving_purpose">

                            <p class="helper-text">What is the purpose of this goal?<br>
                                (Summer vacation, a new car, wedding, a new phone)</p>
                        </div>
                        <button disabled type="button" class="next-btn btn js-next" id="first_button">
                            <i class="pe-7s-angle-right"></i>
                        </button>
                    </fieldset>
                    <fieldset class="js-fieldset">
                        <div class="h2">Start saving towards your goal now</div>
                        <div class="large-icon-container">
                            <i class="pe-7s-cash"></i>

                            <div class="h3">Target Amount</div>
                        </div>
                        <div class="form-group inner-addon">
                            <div>
                                <span class="form-icon" id="naira-icon">&#8358;</span>
                            </div>
                            <input type="number" class="form-control"
                                   placeholder="100,000"  required="required" onkeyup="check_how_much(this.value)" id="target_amt">

                            <p class="helper-text">How much do you plan to save?</p>
                        </div>

                        <button type="button" class="btn previous-btn js-previous">
                            <i class="pe-7s-angle-left"></i>
                        </button>
                        <button disabled type="button" class="btn next-btn js-next" id="second_button">
                            <i class="pe-7s-angle-right"></i>
                        </button>
                    </fieldset>

                    <fieldset class="js-fieldset">
                        <div class="h2">Start saving towards your goal now</div>
                        <div class="large-icon-container">
                            <i class="pe-7s-date"></i>

                            <div class="h3">Target Timeline</div>
                        </div>
                        <div class="form-group inner-addon">
                            <div>
                                <i class="icon ion-ios-calendar-outline form-icon" id="calender-icon"></i>
                            </div>
                            <select class="form-control" id="timeline-select"  required="required">
                                <option value="1">1 month</option>
                                <option value="2" >2 months</option>
                                <option value="3" selected>3 months</option>
                                <option value="4">4 months</option>
                                <option value="5">5 months</option>
                                <option value="6">6 months</option>
                                <option value="7">7 months</option>
                                <option value="8">8 months</option>
                                <option value="9">9 months</option>
                                <option value="10">10 months</option>
                                <option value="11">11 months</option>
                                <option value="12">12 months</option>
                            </select>

                            <p class="helper-text">What's your timeline for this goal?</p>
                        </div>

                        <div class="js-no-timeline">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group inner-addon">
                                        <label class="pull-left">I want to contribute</label>

                                        <div>
                                            <span class="form-icon" id="contribute-icon">&#8358;</span>
                                        </div>
                                        <input type="number" class="form-control"
                                               placeholder="100"  required="required">

                                        <p class="text-left helper-text">How much do you plan to contribute to this
                                            goal?</p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group inner-addon">
                                        <label class="pull-left">How Frequent</label>
                                        <select class="form-control" id="custom-frequency"  required="required">
                                            <option value="daily">Daily</option>
                                            <option value="weekly">Weekly</option>
                                            <option value="monthly">Monthly</option>
                                        </select>

                                        <p class="text-left helper-text">How frequent do you want to save?</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn back-btn js-previous">
                            <span>Back</span>
                        </button>

                        <button type="button" onclick="create()" class="btn create-plan-btn js-next">
                            <span>Create Plan</span>
                        </button>
                    </fieldset>

                    <fieldset class="js-fieldset" style="    margin-top: -12%;" id="s_report">
                      
                    </fieldset>
                </form>

            </div>
        </div>
    </div>
</div>
