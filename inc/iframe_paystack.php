<?php
require_once('../include/check.php'); confirmlogin();

$amount = $_GET['amount'];
$selected_frequency = $_GET['selected_frequency'];
$email = $_SESSION['email'];

$_SESSION['saving_amount'] = $amount;
$_SESSION['selected_frequency'] = $selected_frequency;

$pur = $_SESSION['saving_for'];
$amt = $_SESSION['saving_towards'];
$time = $_SESSION['saving_timeline'];

$paystack_url="";


$digits = '5';
$second_num = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
$order_gen = "Tra".$second_num;


$url = 'https://api.paystack.co/transaction/initialize';

$invoice_no = $order_gen;
$protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') ===
FALSE ? 'http' : 'https';            // Get protocol HTTP/HTTPS
$host     = $_SERVER['HTTP_HOST'];   // Get  www.domain.com
$script   = "/demo/kolowise/inc/savings_payment.php"; // Get folder/file.php
$params   = $_SERVER['QUERY_STRING'];// Get Parameters occupation=odesk&name=ashik

$currentUrl = $protocol . '://' . $host . $script; // Adding all

$callback_url = $currentUrl;




$fields = json_encode(array(
    'reference' => $invoice_no,
    'email' => $email,
    'amount' => ($amount * 100),
    'callback_url' => $currentUrl
));

$live_key = "sk_live_86e8dc6e895b21d6393c8ade8ee0771b475a736c";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, count($fields));
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$headers = array();
$headers[] = 'Authorization: Bearer '.$live_key;
$headers[] = 'Content-Type: application/json;';

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);

curl_close($ch);

$dec_result = json_decode($result); //this is used to decode the json response message

$userid=$_SESSION['usrid'];



$paystack_url = $dec_result->data->authorization_url;

if(!empty($paystack_url)){

	// Create connection
	$conn = new mysqli(DB_HOST,DB_USER, DB_PASS, DB_NAME);
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}

	$sql = "INSERT INTO payment (userid,transactionid,amount) VALUES('$userid','$order_gen','$amount')";

	if ($conn->query($sql) === TRUE) {
		$response = array(
			'status'=>'success',
			'paystack_url'=>$paystack_url,
			'msg'=>'You will be redirected to make payment'
		);
	}


} else {
	$response = array(
		'status'=>'failed',
		'msg'=>'An error occurred while initiating payment. Please try again'
	);
}




echo json_encode($response);





?>

