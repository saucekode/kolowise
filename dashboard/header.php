<?php
/**
 * Created by PhpStorm.
 * User: Afeez
 * Date: 2/8/2017
 * Time: 3:32 PM
 */

require_once('../include/check.php'); confirmlogin();
dashboard();
 getsavingsplan();
 getsavingsplan2();
 getmysavingsplan();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0 width=device-width">
    <title>Dashboard</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/animate.min.css">
    <link rel="stylesheet" href="../css/ionicons.min.css">
    <link rel="stylesheet" href="../css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="../css/sweetalert.css">
    <link rel="stylesheet" href="../css/pikaday.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/dashboard.css">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Iceland" rel="stylesheet">
</head>
<body onload="chk('ghostgeneral-hello')">

<nav class="navbar navbar-default navbar-fixed-top " id="dashboard-navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-"
                    aria-expanded="false" id="nav-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar" style="width: 15px"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" id="app-logo-dashboard" href="../index.php">kolowize</a>
        </div>

        <div class="collapse navbar-collapse" id="navbar-collapse-">
            <ul class="nav navbar-nav" id="dashboard-main-nav">
                <li><a class="nav-links" href="../dashboard"><i class="pe-7s-graph1"></i> Dashboard</a></li>
                <li role="separator" class="mobile-link-divider"></li>
                <li><a class="nav-links" href="mysavings.php"><i class="pe-7s-server"></i> My Savings</a>
                </li>
                <li role="separator" class="mobile-link-divider"></li>
                <li><a class="nav-links" href="#"><i class="pe-7s-gift"></i> My Rewards</a></li>
                <li role="separator" class="mobile-link-divider"></li>
                <li><a class="nav-links js-overlay-open" href="#"><i class="pe-7s-wallet"></i> New Savings Plan</a></li>
                <li role="separator" class="mobile-link-divider"></li>
                <li class="hidden-lg hidden-md hidden-sm"><a href="#" class="nav-links"><i class="pe-7s-config"></i>
                        Settings</a></li>
                <li role="separator" class="mobile-link-divider"></li>
                <li class="hidden-lg hidden-md hidden-sm"><a href="../logout" class="nav-links"><i
                            class="pe-7s-power"></i> Sign
                        Out</a></li>

            </ul>
            <ul class="nav navbar-nav navbar-right hidden-xs" id="dashboard-sec-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <i class="pe-7s-user"></i> Hi <?php echo ucwords($_SESSION['namefull']);?>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="settings.php" class="sec-nav-links"><i class="pe-7s-config"></i>
                                Settings</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="../logout" class="sec-nav-links"><i class="pe-7s-power"></i> Sign Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<main>