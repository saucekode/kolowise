<?php require_once('header.php');?>
<main>
    <div class="activity-summary">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 stats">
                    <div class="text-center">
                        <div class="h3 text-uppercase">- Available balance -</div>
                        <p class="available-balance"><span>&#8358;</span> <?php echo number_format($total_amount, 2);?></p>

                        <p>total saved so far &#8358;  <?php  echo number_format($targetamount);?></p>
                    </div>
                </div>
                <div class="col-sm-4 stats">
                    <div class="text-center">
                        <div class="h3 text-uppercase">- Current Goal -</div>
                        <p class="available-balance"><span>&#8358;</span> <?php  echo number_format($targetamount);?></p>

                        <a class="js-side-modal-open stats-btn"><?php  echo $purpose;?></a>

                    </div>
                </div>
                <div class="col-sm-4 stats">
                    <div class="text-center">
                        <div class="h3 text-uppercase">- Next Milestone -</div>
                        <p class="available-balance"><?php  echo $next_pay_day;?></p>

                        <p>6 milestones to go</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="savings-content">
        <div class="page-heading">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="h2">My Savings</div>
                    </div>
                    <div class="col-sm-4 col-xs-12 pull-right">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search Transactions">

                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default">Go</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
            <?php echo $savingsplan3;?>
             </div>
        </div>
    </div>

    <div class="side-modal" id="side-overlay">
        <a href="javascript:void(0)" class="close-btn js-close-side-modal"><i class="pe-7s-close"></i></a>
        <a href="javascript:void(0)" class="back-btn js-back-btn"><i class="pe-7s-angle-left"></i></a>

        <div id="side-modal-content">

            <div class="container side-modal-container">
                <div class="side-modal-heading text-center">
                    <div class="h3">Summer vacation to Paris</div>
                </div>
                <div class="side-modal-divider"></div>

                <div class="plan-details">
                    <div class="row stats-list">
                        <div class="col-xs-6 stats-list-item">
                            <span class="title"><i class="pe-7s-info"></i> Status</span>
                        </div>

                        <div class="col-xs-6 text-right stats-list-item">
                            <span>Active</span>

                        </div>
                    </div>
                    <div class="row stats-list">
                        <div class="col-xs-6 stats-list-item">
                            <span class="title"><i class="pe-7s-ticket"></i> Reference</span>
                        </div>

                        <div class="col-xs-6 text-right stats-list-item">
                            <span>2LFCKOB</span>

                        </div>
                    </div>
                    <div class="row stats-list">
                        <div class="col-xs-6 stats-list-item">
                            <span class="title"><i class="pe-7s-cash"></i> Amount</span>
                        </div>

                        <div class="col-xs-6 text-right stats-list-item">
                            <span>&#8358; 125,000</span>

                        </div>
                    </div>
                    <div class="row stats-list">
                        <div class="col-xs-6 stats-list-item">
                            <span class="title"><i class="pe-7s-clock"></i> Started</span>
                        </div>

                        <div class="col-xs-6 text-right stats-list-item">
                            <span class="text-uppercase">jan 23, 2017</span>

                        </div>
                    </div>
                    <div class="row stats-list">
                        <div class="col-xs-6 stats-list-item">
                            <span class="title"><i class="pe-7s-date"></i> Duration</span>
                        </div>

                        <div class="col-xs-6 text-right stats-list-item">
                            <span class="text-capitalize">8 months</span>

                        </div>
                    </div>
                    <div class="row stats-list">
                        <div class="col-xs-6 stats-list-item">
                            <span class="title"><i class="pe-7s-piggy"></i> Saved</span>
                        </div>

                        <div class="col-xs-6 text-right stats-list-item">
                            <span>&#8358; 25,000</span>

                        </div>
                    </div>
                    <div class="row stats-list">
                        <div class="col-xs-6 stats-list-item">
                            <span class="title"><i class="pe-7s-date"></i> Next Deposit</span>
                        </div>

                        <div class="col-xs-6 text-right stats-list-item">
                            <span class="text-uppercase">feb 23, 2017</span>

                        </div>
                    </div>
                    <div class="row stats-list">
                        <div class="col-xs-8 stats-list-item">
                            <span class="title"><i class="pe-7s-medal"></i> Milestones Completed</span>
                        </div>

                        <div class="col-xs-4 text-right stats-list-item">
                            <span>5</span>

                        </div>
                    </div>

                    <div class="row stats-list">
                        <div class="col-xs-6 stats-list-item">
                            <span class="title"><i class="pe-7s-gift"></i> Rewards</span>
                        </div>

                        <div class="col-xs-6 text-right stats-list-item">
                            <span>5</span>

                        </div>
                    </div>
                    <div class="goal-timeline">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="h4">
                                    <i class="icon ion-android-options"></i>
                                    <span>Milestones</span>
                                </div>
                            </div>
                        </div>
                        <hr style="margin: 0">
                        <div class="row stats-list">
                            <div class="col-xs-12">
                                <p class="title">Your next deposit is on Jan, 23 2017. Meet target for next deposit and
                                    win a ticket to watch a movie of your choice at FilmHouse cinema</p>
                            </div>
                        </div>
                        <div class="row stats-list">
                            <div class="col-xs-12">
                                <p class="title">Your next deposit is on Jan, 23 2017. Meet target for next deposit and
                                    win a ticket to watch a movie of your choice at FilmHouse cinema</p>
                            </div>
                        </div>
                        <div class="row stats-list">
                            <div class="col-xs-12">
                                <p class="title">Your next deposit is on Jan, 23 2017. Meet target for next deposit and
                                    win a ticket to watch a movie of your choice at FilmHouse cinema</p>
                            </div>
                        </div>
                    </div>
                    <div class="row action-list">
                        <div class="col-xs-12" style="padding: 0">
                            <button type="button" class="btn withdraw-btn-danger pull-right js-withdraw-btn">
                                <i class="icon ion-eject"></i> Withdraw
                            </button>
                        </div>
                    </div>
                </div>

                <div class="withdraw-form-container">
                    <form class="withdraw-form">
                        <div class="form-group inner-addon">
                            <label>Withdraw option</label>
                            <select class="form-control" id="withdraw-option">
                                <option value="1">Withdraw All</option>
                                <option value="2">Withdraw specific amount</option>
                            </select>

                        </div>
                        <div class="form-group inner-addon" id="custom-withdraw-amount">
                            <label>Amount to withdraw</label>

                            <div>
                                <span class="form-icon" id="withdraw-naira-icon">&#8358;</span>
                            </div>
                            <input type="number" class="form-control"
                                   placeholder="10,000">
                        </div>
                        <button type="submit" class="btn withdraw-btn-success pull-right js-withdraw-confirm">
                            <i class="icon ion-eject"></i> Withdraw
                        </button>
                    </form>
                </div>
            </div>

        </div>

    </div>



<?php require_once('footer.php');?>