<?php require_once('header.php');?>

    <div class="activity-summary">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 stats">
                    <div class="text-center">
                        <div class="h3 text-uppercase">- Available balance -</div>
                        <p class="available-balance"><span>&#8358;</span> <?php echo number_format($total_amount, 2);?></p>

                        <p>total saved so far &#8358; <?php  echo number_format($targetamount);?></p>
                    </div>
                </div>
                <div class="col-sm-4 stats">
                    <div class="text-center">
                        <div class="h3 text-uppercase">- Current Goal -</div>
                        <p class="available-balance"><span>&#8358;</span> <?php  echo number_format($targetamount);?></p>

                        <a href="dashboard_my_savings.html" class="stats-btn"><?php  echo $purpose;?></a>

                    </div>
                </div>
                <div class="col-sm-4 stats">
                    <div class="text-center">
                        <div class="custom-label active text-uppercase"><i
                                class="icon ion-ios-circle-outline"></i> Active
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <p id="stats-small-text"><i class="pe-7s-piggy"></i><?php  echo $purpose;?></p>
                            </div>
                        </div>
                        <div class="row info-list">
                            <div class="col-xs-6 info-list-item">
                                <span><i class="pe-7s-clock"></i> Started</span>
                            </div>
                            <div class="col-xs-6 text-right info-list-item">
                                <span class="text-uppercase"><?php  echo $startdate;?></span>

                            </div>
                        </div>
                        <div class="row info-list">
                            <div class="col-xs-6 info-list-item">
                                <span><i class="pe-7s-date"></i> Next Deposit</span>
                            </div>
                            <div class="col-xs-6 text-right info-list-item">
                                <span class="text-uppercase"><?php  echo $next_pay_day;?></span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="dashboard-active-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="dashboard-summary">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="h3">
                                    <span>My savings</span>
                                    <a href="dashboard_settings.html" class="settings-icon pull-right"><i
                                            class="icon ion-android-settings"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <?php echo $savingsplan; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="dashboard-summary">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="h3">
                                    <span>Bank Accounts</span>
                                    <a href="dashboard_settings.html" class="settings-icon pull-right"><i
                                            class="icon ion-android-settings"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                               <!-- <a href="dashboard_settings.html" class="bank-details-summary">
                                    <div class="row">
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="summary-bank-name">
                                                <p><i class="icon ion-record active"></i> Access Bank</p>
                                            </div>

                                        </div>
                                        <div class="col-sm-4 col-xs-12 pull-right">
                                            <div class="summary-account-number">
                                                <p>2147483647</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <a href="dashboard_settings.html" class="bank-details-summary">
                                    <div class="row">
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="summary-bank-name">
                                                <p><i class="icon ion-record inactive"></i> Diamond Bank</p>
                                            </div>

                                        </div>
                                        <div class="col-sm-4 col-xs-12 pull-right">
                                            <div class="summary-account-number">
                                                <p>2147483647</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>-->
                                Not Avaliable
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="dashboard-summary">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="h3">
                                    <span>My Rewards</span>
                                    <a href="dashboard_settings.html" class="settings-icon pull-right"><i
                                            class="icon ion-android-settings"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                            <!--    <a href="dashboard_my_savings.html" class="dashboard-savings-summary">
                                    <div class="row">
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="summary-savings-name">
                                                <p><i class="pe-7s-piggy"></i> Summer vacation to Paris</p>
                                            </div>

                                        </div>
                                        <div class="col-sm-4 col-xs-12 pull-right">
                                            <div class="summary-savings-amount">
                                                <span class="active">&#8358; 5000</span>
                                                <span>available &#8358; 0.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <a href="dashboard_my_savings.html" class="dashboard-savings-summary">
                                    <div class="row">
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="summary-savings-name">
                                                <p><i class="pe-7s-piggy"></i> A new car</p>
                                            </div>

                                        </div>
                                        <div class="col-sm-4 col-xs-12 pull-right">
                                            <div class="summary-savings-amount">
                                                <span class="inactive">&#8358; 5000</span>
                                                <span>available &#8358; 0.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>-->
                                Not Avaliable
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div class="dashboard-details">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="savings-summary-details">
                                    <div class="row">
                                        <div class="col-sm-4 col-xs-12">
                                            <form style="margin-bottom: 10px">
                                                <div class="form-group inner-addon">
                                                    <div>
                                                        <i class="pe-7s-angle-down form-icon"></i>
                                                    </div>
                                                    <input class="form-control" id="datepicker"
                                                           placeholder="Select date">
                                                </div>
                                            </form>

                                        </div>
                                        <div class="col-sm-8 col-xs-12 pull-right">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="monthly-savings-summary text-center">
                                                        <div class="h3">&#8358; 5000</div>
                                                        <p>Average monthly savings</p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <a href="dashboard_my_savings.html"
                                                       class="total-savings-summary text-center">
                                                        <div class="h3">2</div>
                                                        <p>Total savings plans</p>
                                                    </a>
                                                </div>
                                                <div class="col-sm-4">
                                                    <a href="#" class="total-rewards-summary text-center">
                                                        <div class="h3">2</div>
                                                        <p>Total rewards earned</p>
                                                    </a>

                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                               <div class="row">


                               <?php echo $savingsplan2;?>

                               </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <a class="view-all-btn" href="mysavings.php">View All Savings</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php require_once("footer.php");?>